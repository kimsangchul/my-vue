package com.sec.pim.hackersnews.model;

public class UserModel {
	private String about;
	
	private int created_time;
	
	private String created;
	
	private String id;
	
	private int karma;

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public int getCreated_time() {
		return created_time;
	}

	public void setCreated_time(int created_time) {
		this.created_time = created_time;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getKarma() {
		return karma;
	}

	public void setKarma(int karma) {
		this.karma = karma;
	}
}
