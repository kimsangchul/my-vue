<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Insert title here</title>
	<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/vuex/3.5.1/vuex.js"></script>
	<style>
		table {
			border-collapse: collapse;
			border : 1px solid black;
		}
		td {
			border-collapse: collapse;
			border : 1px solid black;
			width : 100px;
			height : 100px;
			text-align : center;
			user-select : none;
		}
	</style>
</head>

<body>
	<div id="app">
		<div>
			<mine-form/>
		</div>
		<div>
			<time-form/>
		</div>
		<div>
			<table-form/>
		</div>
	</div>
</body>
<script type="text/javascript">
	var interval = null;
	
	var CODE = {
		NORMAL : -1,
		QUESTION : -2,
		FLAG : -3,
		MINE : -4,
		QUESTION_MINE : -5,
		FLAG_MINE : -6,
		CLICK_NORMAL : -7,
		CLICK_MINE : -8,
	}

	function getRandomNumber(data) {
		var arr = Array(data.row * data.col).fill().map(function(v, idx){
			return idx;
		});
		
		var newArr = [];
		for(var i = 0; i < data.mine; i++) {
			newArr.push(arr.splice(Math.floor(Math.random() * arr.length), 1)[0]);
		}
		return newArr;
	}
	
	var MineForm = Vue.component('MineForm', {
		template : `<div>
						<input type="text" ref="row" :value="row"/>
						<input type="text" ref="col" :value="col"/>
						<input type="text" ref="mine" :value="mine"/>
						<button @click="onSubmitBtn">시작</button>
					</div>`,
		data() {
			return {
				row : 10,
				col : 10,
				mine : 20,
			}
		},
		methods : {
			onSubmitBtn() {
				var row = parseInt(this.$refs.row.value);
				var col = parseInt(this.$refs.col.value);
				var mine = parseInt(this.$refs.mine.value);
				this.$store.commit('SET_TABLE_DATA', {row : row, col : col, mine : mine});
			},
		},
	});
	
	var TimeForm = Vue.component('TimeForm', {
		template : `<div>
						{{ getTimeout }} ms
					</div>`,
		watch : {
			getHalted(value) {
				var $this = this;
				
				if(value) {
					clearInterval(interval);
				} else {
					var i = 0;
					interval = setInterval(function(){
						i++;
						$this.$store.commit('SET_TIMEOUT', i);
					}, 1000);
				}
			}
		},
		computed : {
			getHalted() {
				return this.$store.getters.getHalted;
			},
			getTimeout() {
				return this.$store.getters.getTimeout;
			}
		}
	});
	
	var TableForm = Vue.component('TableForm', {
		template : `<div>
						<table>
							<tr v-for="(rowData, rowIdx) in getTableData">
								<td v-for="(colData, colIdx) in rowData" :style="styleObj(rowIdx, colIdx)" @click="onClickTd(rowIdx, colIdx)" @contextmenu.prevent="onRightTd(rowIdx, colIdx)">
									{{ textObj(colData) }}
								</td>
							</tr>
						</table>
					</div>`,
		computed : {
			getTableData() {
				return this.$store.getters.getTableData;
			},
			styleObj(rowIdx, colIdx) {
				return function(rowIdx, colIdx) {
					switch(this.getTableData[rowIdx][colIdx]) {
						case CODE.NORMAL :
						case CODE.MINE :
							return {
								backgroundColor : 'gray',
							}
						case CODE.QUESTION :
						case CODE.QUESTION_MINE :
							return {
								backgroundColor : 'blue',
							}
						case CODE.FLAG :
						case CODE.FLAG_MINE :
							return {
								backgroundColor : 'red',
							}
						default :
							return;
					}
				}
			},
			textObj(colData) {
				return function(colData) {
					switch(colData) {
						case CODE.NORMAL :
						case CODE.CLICK_NORMAL :
							return '';
						case CODE.QUESTION :
						case CODE.QUESTION_MINE :
							return '?';
						case CODE.FLAG :
						case CODE.FLAG_MINE :
							return '!';
						case CODE.MINE :
							return 'X';
						case CODE.CLICK_MINE :
							return '펑';
						default :
							return colData || '';
					}
				}
			}
		},
		methods : {
			onClickTd(rowIdx, colIdx) {
				switch(this.getTableData[rowIdx][colIdx]) {
					case CODE.NORMAL :
					case CODE.QUESTION :
					case CODE.FLAG :
						this.$store.commit('CLICK_NORMAL', {row : rowIdx, col : colIdx});
						break;
					case CODE.MINE :
					case CODE.QUESTION_MINE :
					case CODE.FLAG_MINE :
						this.$store.commit('CLICK_MINE', {row : rowIdx, col : colIdx});
						break;
					default :
						break;
				}
			},
			onRightTd(rowIdx, colIdx) {
				switch(this.getTableData[rowIdx][colIdx]) {
					case CODE.NORMAL :
					case CODE.MINE :
						this.$store.commit('QUESTION_CELL', {row : rowIdx, col : colIdx});
						break;
					case CODE.QUESTION :
					case CODE.QUESTION_MINE :
						this.$store.commit('FLAG_CELL', {row : rowIdx, col : colIdx});
						break;
					case CODE.FLAG :
					case CODE.FLAG_MINE :
						this.$store.commit('NORMAL_CELL', {row : rowIdx, col : colIdx});
						break;
					default :
						break;
				}
			},
		}
	});
	
	var store = new Vuex.Store({
		state : {
			tableData : [],
			halted : true,
			timeout : 0,
			checked : [],
		},
		mutations : {
			SET_TABLE_DATA(state, data) {
				var tableData = [];
				for(var i = 0; i < data.row; i++) {
					var rowData = [];
					for(var j = 0; j < data.col; j++) {
						rowData.push(CODE.NORMAL);
					}
					tableData.push(rowData);
				}				
				state.tableData = tableData;
				state.halted = false;
				state.timeout = 0;
				state.checked = [];
				
				if(interval) {
					clearInterval(interval);
					var j = 0;
					interval = setInterval(function(){
						j++;
						state.timeout = j;
					}, 1000);
				}
				
				var mineArr = getRandomNumber(data);
				for(var i = 0; i < mineArr.length; i++) {
					var moc = parseInt(mineArr[i] / 10);
					var mod = mineArr[i] % 10;
					
					Vue.set(state.tableData[moc], mod, CODE.MINE);
				}
			},
			SET_TIMEOUT(state, timeout) {
				state.timeout = timeout;
			},
			CLICK_NORMAL(state, data) {
				function clickCell(state, data) {
					state.checked.push(data.row + '/' + data.col);
					
					var around = [];
					
					if(data.row-1 >= 0 && data.row-1 <= 9 && data.col-1 >= 0 && data.col-1 <= 9) {
						around.push(state.tableData[data.row-1][data.col-1]);
					}
					if(data.row-1 >= 0 && data.row-1 <= 9 && data.col >= 0 && data.col <= 9) {
						around.push(state.tableData[data.row-1][data.col]);
					}
					if(data.row-1 >= 0 && data.row-1 <= 9 && data.col+1 >= 0 && data.col+1 <= 9) {
						around.push(state.tableData[data.row-1][data.col+1]);
					}
					if(data.row >= 0 && data.row <= 9 && data.col-1 >= 0 && data.col-1 <= 9) {
						around.push(state.tableData[data.row][data.col-1]);
					}
					if(data.row >= 0 && data.row <= 9 && data.col+1 >= 0 && data.col+1 <= 9) {
						around.push(state.tableData[data.row][data.col+1]);
					}
					if(data.row+1 >= 0 && data.row+1 <= 9 && data.col-1 >= 0 && data.col-1 <= 9) {
						around.push(state.tableData[data.row+1][data.col-1]);
					}
					if(data.row+1 >= 0 && data.row+1 <= 9 && data.col >= 0 && data.col <= 9) {
						around.push(state.tableData[data.row+1][data.col]);
					}
					if(data.row+1 >= 0 && data.row+1 <= 9 && data.col+1 >= 0 && data.col+1 <= 9) {
						around.push(state.tableData[data.row+1][data.col+1]);
					}
					
					var mineIdx = 0;
					around.filter(function(v, idx){
						if(v === CODE.MINE || v === CODE.QUESTION_MINE || v === CODE.FLAG_MINE) {
							mineIdx++;
						}
					});
					
					Vue.set(state.tableData[data.row], data.col, mineIdx);
					
					// 클릭한 셀 주변에 지뢰가 없으면
					if(mineIdx === 0) {
						var near = [];
						
						if(data.row-1 >= 0 && data.row-1 <= 9 && data.col-1 >= 0 && data.col-1 <= 9) {
							near.push([data.row-1, data.col-1]);
						}
						if(data.row-1 >= 0 && data.row-1 <= 9 && data.col >= 0 && data.col <= 9) {
							near.push([data.row-1, data.col]);
						}
						if(data.row-1 >= 0 && data.row-1 <= 9 && data.col+1 >= 0 && data.col+1 <= 9) {
							near.push([data.row-1, data.col+1]);
						}
						if(data.row >= 0 && data.row <= 9 && data.col-1 >= 0 && data.col-1 <= 9) {
							near.push([data.row, data.col-1]);
						}
						if(data.row >= 0 && data.row <= 9 && data.col+1 >= 0 && data.col+1 <= 9) {
							near.push([data.row, data.col+1]);
						}
						if(data.row+1 >= 0 && data.row+1 <= 9 && data.col-1 >= 0 && data.col-1 <= 9) {
							near.push([data.row+1, data.col-1]);
						}
						if(data.row+1 >= 0 && data.row+1 <= 9 && data.col >= 0 && data.col <= 9) {
							near.push([data.row+1, data.col]);
						}
						if(data.row+1 >= 0 && data.row+1 <= 9 && data.col+1 >= 0 && data.col+1 <= 9) {
							near.push([data.row+1, data.col+1]);
						}
						
						for(var k = 0; k < near.length; k++) {
							// 아직 한번도 검사하지 않은 셀이면
							if(state.checked.indexOf(near[k][0] + '/' + near[k][1]) < 0) {
								clickCell(state, {row : near[k][0], col : near[k][1]});	
							}
						}
					}
					
					if(state.checked.length === 80) {
						alert('성공');
						state.halted = true;
					}
				}
				
				if(state.halted) {
					return;
				}
				
				clickCell(state, data);
			},
			CLICK_MINE(state, data) {
				if(state.halted) {
					return;
				}
				
				Vue.set(state.tableData[data.row], data.col, CODE.CLICK_MINE);
				state.halted = true;
			},
			NORMAL_CELL(state, data) {
				if(state.halted) {
					return;
				}
				
				if(state.tableData[data.row][data.col] === CODE.FLAG) {
					Vue.set(state.tableData[data.row], data.col, CODE.NORMAL);
				} else {
					Vue.set(state.tableData[data.row], data.col, CODE.MINE);
				}
			},
			QUESTION_CELL(state, data) {
				if(state.halted) {
					return;
				}
				
				if(state.tableData[data.row][data.col] === CODE.NORMAL) {
					Vue.set(state.tableData[data.row], data.col, CODE.QUESTION);
				} else {
					Vue.set(state.tableData[data.row], data.col, CODE.QUESTION_MINE);
				}
			},
			FLAG_CELL(state, data) {
				if(state.halted) {
					return;
				}
				
				if(state.tableData[data.row][data.col] === CODE.QUESTION) {
					Vue.set(state.tableData[data.row], data.col, CODE.FLAG);
				} else {
					Vue.set(state.tableData[data.row], data.col, CODE.FLAG_MINE);
				}
			},
		},
		actions : {
			
		},
		getters : {
			getTableData(state) {
				return state.tableData;
			},
			getHalted(state) {
				return state.halted;
			},
			getTimeout(state) {
				return state.timeout;
			}
		},
	});
	
	new Vue({
		el : '#app',
		store : store,
	});
	
</script>
</html>