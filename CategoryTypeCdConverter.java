/**
 * Copyright (c) 2011 Samsung Electronics. All Rights Reserved.
 * Project: Samsung.com Site Renewal(2011.03~08)
 */
package com.sec.pim.hackersnews.enums.converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.core.convert.converter.Converter;

import com.sec.pim.hackersnews.enums.CategoryType;

/**
 *  Class Name : NoticeMapTypeCdConverter.java
 *  Description : 
 *  Modification Information
 * 
 *   수정일           		 수정자    			수정내용
 *   -------  		 -----   	 	-------- 
 *   2020. 4. 2.	 jungho1.noh    최초 생성
 *
 *  @author jungho1.noh
 *  @since 2020. 4. 2.
 *  @version 1.0
*/
public class  CategoryTypeCdConverter implements Converter<String, CategoryType> {
 
    @Override
    public CategoryType convert(String arg0) {
    	 
         for (CategoryType item: CategoryType.values()) {           
             if (StringUtils.equals(item.getCode(),arg0)) {
                 return item;
             }
         }
         return null;
    	
    }
 
}