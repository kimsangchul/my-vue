import axios from 'axios';

const context = {
	root : '/admin',
}

function categoriesUrl() {
	return axios.get(`${context.root}/getCategories`);
}

function insertNewsUrl() {
	return axios.get(`${context.root}/insertNews`);
}

function insertAskUrl() {
	return axios.get(`${context.root}/insertAsk`);
}

function insertJobsUrl() {
	return axios.get(`${context.root}/insertJobs`);
}

function selectListUrl(params) {
	return axios.get(`${context.root}/selectList`, {
		params,
	});
}

function selectUserUrl(params) {
	return axios.get(`${context.root}/selectUser`, {
		params,
	});
}

function selectHackersNewsUrl(params) {
	return axios.get(`${context.root}/selectHackersNews`, {
		params,
	});
}

function selectItemUrl(params) {
	return axios.get(`${context.root}/selectItem`, {
		params,
	});
}

function mergeUserUrl(params) {
	return axios.get(`${context.root}/mergeUser`, {
		params,
	});
}

function mergeHackersNewsUrl(params) {
	return axios.get(`${context.root}/mergeHackersNews`, {
		params,
	});
}

function mergeItemUrl(params) {
	return axios.get(`${context.root}/mergeItem`, {
		params,
	});
}

function userApiUrl(name) {
	return axios.get(`https://api.hnpwa.com/v0/user/${name}.json`);
}

function itemApiUrl(id) {
	return axios.get(`https://api.hnpwa.com/v0/item/${id}.json`);
}

export {
	categoriesUrl,
	insertNewsUrl,
	insertAskUrl,
	insertJobsUrl,
	selectListUrl,
	selectUserUrl,
	selectHackersNewsUrl,
	selectItemUrl,
	mergeUserUrl,
	mergeHackersNewsUrl,
	mergeItemUrl,
	userApiUrl,
	itemApiUrl,
}