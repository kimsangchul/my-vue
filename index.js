import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import axios from 'axios';

export default new Vuex.Store({
	state : {
		totalRows : 0,
	},
	mutations : {
		SET_TOTAL_ROWS(state, totalRows) {
			state.totalRows = totalRows;
		},
	},
	actions : {
		async FETCH_HACKERSNEWS(context, params) {
			const response = await axios.get('/admin/selectNews', {
				params,
			});
			
			const totalRows = response.data.c;
			context.commit('SET_TOTAL_ROWS', totalRows);
			const list = JSON.parse(response.data.d);
			return list;
		},
	},
	getters : {
		getTotalRows(state) {
			return state.totalRows;
		},
	},
});