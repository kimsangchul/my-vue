package com.sec.pim.hackersnews.model;

import java.util.List;

public class PageModel {
	private int rows;
	
	private int page;
	
	private int records;
	
	private String sortBy;
	
	private String sortDesc;
	
	private List<String> filterOn;
	
	private String filter;

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public String getSortDesc() {
		return sortDesc;
	}

	public void setSortDesc(String sortDesc) {
		this.sortDesc = sortDesc;
	}

	public List<String> getFilterOn() {
		return filterOn;
	}

	public void setFilterOn(List<String> filterOn) {
		this.filterOn = filterOn;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}
}
