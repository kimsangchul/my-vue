package com.sec.pim.hackersnews.enums;

import org.apache.ibatis.type.MappedTypes;
import org.codehaus.jackson.annotate.JsonValue;

public enum CategoryType implements CodeEnum {

	News("01", "News"), 
	Ask("02", "Ask"), 
	Jobs("03", "Jobs");

	private String code;
	private String name;

	CategoryType(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@MappedTypes(CategoryType.class)
	public static class TypeHandler extends CodeEnumTypeHandler<CategoryType> {
		public TypeHandler() {
			super(CategoryType.class);
		}
	}

	@Override
	@JsonValue
	public String getCode() {
		return code;
	}
}