package com.sec.pim.hackersnews.enums;

public interface CodeEnum {
	String getCode();
}