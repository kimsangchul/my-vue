import Vue from 'vue'
import App from './App.vue'

import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import router from '../../routes/index.js';
import store from '../../store/index.js';

Vue.use(BootstrapVue)
Vue.config.productionTip = false

import VueSwal from 'vue-swal';
Vue.use(VueSwal);

new Vue({
  render: h => h(App),
  router,
  store,
}).$mount('#app')
