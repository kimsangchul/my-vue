package com.sec.pim.hackersnews.dao;

import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sec.pim.hackersnews.model.HackersNewsModel;
import com.sec.pim.hackersnews.model.ItemModel;
import com.sec.pim.hackersnews.model.UserModel;

@Repository
public class HackersNewsDao {
	
	@Autowired
    private SqlSessionTemplate sqlSession;
	
	public List<HackersNewsModel> selectList(HackersNewsModel model) {
		return sqlSession.selectList("hackersnews.selectList", model);
	}
	
	public void mergeHackersNews(HackersNewsModel hackersNews) {
		sqlSession.update("hackersnews.mergeHackersNews", hackersNews);
	}

	public List<Map<String, String>> getCategoryType() {
		return sqlSession.selectList("hackersnews.getCategoryType");
	}

	public void mergeUser(UserModel model) {
		sqlSession.update("hackersnews.mergeUser", model);
	}

	public void mergeItem(ItemModel model) {
		sqlSession.update("hackersnews.mergeItem", model);
	}

	public UserModel selectUser(UserModel model) {
		return sqlSession.selectOne("hackersnews.selectUser", model);
	}
	
	public HackersNewsModel selectHackersNews(HackersNewsModel model) {
		return sqlSession.selectOne("hackersnews.selectHackersNews", model);
	}
	
	public ItemModel selectItem(ItemModel model) {
		return sqlSession.selectOne("hackersnews.selectItem", model);
	}
}
