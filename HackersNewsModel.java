package com.sec.pim.hackersnews.model;

import com.sec.pim.hackersnews.enums.CategoryType;

public class HackersNewsModel extends PageModel {
	private int id;
	
	private String title;
	
	private int points;
	
	private String name;
	
	private int time;
	
	private String time_ago;
	
	private int comments_count;
	
	private String type;
	
	private String url;
	
	private String domain;
	
	private CategoryType category;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public String getTime_ago() {
		return time_ago;
	}

	public void setTime_ago(String time_ago) {
		this.time_ago = time_ago;
	}

	public int getComments_count() {
		return comments_count;
	}

	public void setComments_count(int comments_count) {
		this.comments_count = comments_count;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public CategoryType getCategory() {
		return category;
	}

	public void setCategory(CategoryType category) {
		this.category = category;
	}
}
