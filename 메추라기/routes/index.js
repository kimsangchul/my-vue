import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import TopComponent from '../components/TopComponent.vue';
import LeftComponent from '../components/LeftComponent.vue';
import CenterComponent from '../components/CenterComponent.vue';
import RightComponent from '../components/RightComponent.vue';
import BottomComponent from '../components/BottomComponent.vue';

import ChangeLeftComponent from '../components/ChangeLeftComponent.vue';

import ItemComponent from '../components/ItemComponent.vue';

export default new VueRouter({
	mode : 'history',
	routes : [
		{
			path : '/admin/item',
			component : ItemComponent,
		},
		{
			path : '/admin/product',
			components : {
				top : TopComponent,
				left : ChangeLeftComponent,
				center : CenterComponent,
				right : RightComponent,
				bottom : BottomComponent,
			}
		},
	],
});