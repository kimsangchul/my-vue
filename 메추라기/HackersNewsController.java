package com.sec.pim.hackersnews.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sec.pim.hackersnews.dao.HackersNewsDao;
import com.sec.pim.hackersnews.enums.CategoryType;
import com.sec.pim.hackersnews.model.HackersNewsModel;
import com.sec.pim.hackersnews.model.ItemModel;
import com.sec.pim.hackersnews.model.UserModel;

@Controller
public class HackersNewsController {
	
	@Autowired
	private HackersNewsDao dao;
	
    @RequestMapping(value="/index", method=RequestMethod.GET)
    public ModelAndView index() {
    	return new ModelAndView("index");
    }
    
    @RequestMapping(value="/item", method=RequestMethod.GET)
    public ModelAndView item() {
    	return new ModelAndView("index");
    }
    
    @RequestMapping(value="/product", method=RequestMethod.GET)
    public ModelAndView product() {
    	return new ModelAndView("product");
    }
    
    @RequestMapping(value="/selectList", method=RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> selectList(HackersNewsModel model) {
    	
    	try {
    		Map<String, Object> map = new HashMap<String, Object>();
        	Gson gson = new Gson();
        	List<HackersNewsModel> hackersnewsList = dao.selectList(model);
        	map.put("c", 0);
        	if(hackersnewsList.stream().findFirst().map(e -> e.getRecords()).isPresent()) {
        		map.put("c", hackersnewsList.stream().findFirst().map(e -> e.getRecords()).get());
        	}
        	map.put("d", gson.toJson(hackersnewsList));
        	
        	return map;
    	} catch(Exception e) {
    		throw new RuntimeException(e);
    	}
    	
    }
    
    @RequestMapping(value="/getCategories", method=RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getCategories() {
    	
    	try {
    		Map<String, Object> map = new HashMap<String, Object>();
        	Gson gson = new Gson();
        	List<Map<String, String>> codeList = dao.getCategoryType();
        	map.put("d", gson.toJson(codeList));
        	
        	return map;
    	} catch(Exception e) {
    		throw new RuntimeException(e);
    	}
    	
    }
    
    @RequestMapping(value="/insertNews", method=RequestMethod.GET)
    public ModelAndView insertNews() {
    	String json = "[{\"id\":24066748,\"title\":\"The creeping scourge of tooling config files in project root directories\",\"points\":215,\"user\":\"flaque\",\"time\":1596670261,\"time_ago\":\"4 hours ago\",\"comments_count\":121,\"type\":\"link\",\"url\":\"https://github.com/nodejs/tooling/issues/79\",\"domain\":\"github.com\"},{\"id\":24066832,\"title\":\"Frances Allen has died\",\"points\":155,\"user\":\"ntumlin\",\"time\":1596671187,\"time_ago\":\"4 hours ago\",\"comments_count\":20,\"type\":\"link\",\"url\":\"https://www.ibm.com/blogs/research/2020/08/remembering-frances-allen/\",\"domain\":\"ibm.com\"},{\"id\":24067840,\"title\":\"The revolutionary boat powered by the ocean\",\"points\":13,\"user\":\"awk\",\"time\":1596683991,\"time_ago\":\"40 minutes ago\",\"comments_count\":0,\"type\":\"link\",\"url\":\"https://www.bbc.com/future/article/20200718-the-revolutionary-electric-boat-powered-by-the-ocean\",\"domain\":\"bbc.com\"},{\"id\":24067365,\"title\":\"$250k books sold, to save lives\",\"points\":44,\"user\":\"gregalbritton\",\"time\":1596677627,\"time_ago\":\"2 hours ago\",\"comments_count\":10,\"type\":\"link\",\"url\":\"https://sive.rs/250k\",\"domain\":\"sive.rs\"},{\"id\":24066997,\"title\":\"Stripe Hires AWS' Mike Clayville as Chief Revenue Officer\",\"points\":92,\"user\":\"simonebrunozzi\",\"time\":1596673033,\"time_ago\":\"4 hours ago\",\"comments_count\":38,\"type\":\"link\",\"url\":\"https://stripe.com/en-au/newsroom/news/mike-clayville\",\"domain\":\"stripe.com\"},{\"id\":24066193,\"title\":\"Arcosanti\",\"points\":142,\"user\":\"simonebrunozzi\",\"time\":1596665615,\"time_ago\":\"6 hours ago\",\"comments_count\":40,\"type\":\"link\",\"url\":\"https://en.wikipedia.org/wiki/Arcosanti\",\"domain\":\"en.wikipedia.org\"},{\"id\":24067611,\"title\":\"American Darknet Vendor and Costa Rican Pharmacist Charged\",\"points\":23,\"user\":\"Fjolsvith\",\"time\":1596680747,\"time_ago\":\"2 hours ago\",\"comments_count\":17,\"type\":\"link\",\"url\":\"https://www.justice.gov/opa/pr/american-darknet-vendor-and-costa-rican-pharmacist-charged-narcotics-and-money-laundering\",\"domain\":\"justice.gov\"},{\"id\":24066570,\"title\":\"Compiler Explorer\",\"points\":96,\"user\":\"stmw\",\"time\":1596668637,\"time_ago\":\"5 hours ago\",\"comments_count\":18,\"type\":\"link\",\"url\":\"https://godbolt.org/\",\"domain\":\"godbolt.org\"},{\"id\":24067264,\"title\":\"Working in Public: The Making and Maintenance of Open Source Software\",\"points\":36,\"user\":\"joelg\",\"time\":1596676018,\"time_ago\":\"3 hours ago\",\"comments_count\":4,\"type\":\"link\",\"url\":\"https://www.amazon.com/dp/0578675862/\",\"domain\":\"amazon.com\"},{\"id\":24065499,\"title\":\"File System Interfaces for Go – Draft Design\",\"points\":107,\"user\":\"networkimprov\",\"time\":1596660961,\"time_ago\":\"7 hours ago\",\"comments_count\":76,\"type\":\"link\",\"url\":\"https://go.googlesource.com/proposal/+/master/design/draft-iofs.md\",\"domain\":\"go.googlesource.com\"},{\"id\":24060437,\"title\":\"Facebook launches its TikTok rival, Instagram Reels\",\"points\":375,\"user\":\"theBashShell\",\"time\":1596633477,\"time_ago\":\"15 hours ago\",\"comments_count\":305,\"type\":\"link\",\"url\":\"https://www.axios.com/facebook-launches-its-tiktok-rival-instagram-reels-56460094-88df-4aa5-b6a1-39695c682508.html\",\"domain\":\"axios.com\"},{\"id\":24059303,\"title\":\"The Hacker Way: How I taught my nephew to program\",\"points\":86,\"user\":\"stopachka\",\"time\":1596624719,\"time_ago\":\"17 hours ago\",\"comments_count\":28,\"type\":\"link\",\"url\":\"https://stopa.io/post/246\",\"domain\":\"stopa.io\"},{\"id\":24046951,\"title\":\"The ‘solar canals’ making smart use of India’s space\",\"points\":12,\"user\":\"palo3\",\"time\":1596519067,\"time_ago\":\"2 days ago\",\"comments_count\":0,\"type\":\"link\",\"url\":\"https://www.bbc.com/future/article/20200803-the-solar-canals-revolutionising-indias-renewable-energy\",\"domain\":\"bbc.com\"},{\"id\":24066873,\"title\":\"Touch Typing on a Gamepad\",\"points\":37,\"user\":\"krisfris\",\"time\":1596671671,\"time_ago\":\"4 hours ago\",\"comments_count\":12,\"type\":\"link\",\"url\":\"https://darkshadow.io/2020/07/07/touch-typing-on-a-gamepad.html\",\"domain\":\"darkshadow.io\"},{\"id\":24061268,\"title\":\"Latest Firefox rolls out Enhanced Tracking Protection 2.0\",\"points\":703,\"user\":\"LinuxBender\",\"time\":1596638382,\"time_ago\":\"13 hours ago\",\"comments_count\":396,\"type\":\"link\",\"url\":\"https://blog.mozilla.org/blog/2020/08/04/latest-firefox-rolls-out-enhanced-tracking-protection-2-0-blocking-redirect-trackers-by-default/\",\"domain\":\"blog.mozilla.org\"},{\"id\":24065794,\"title\":\"What is Jio, and why are tech’s biggest players suddenly obsessed with it?\",\"points\":47,\"user\":\"simonebrunozzi\",\"time\":1596662748,\"time_ago\":\"7 hours ago\",\"comments_count\":32,\"type\":\"link\",\"url\":\"https://onezero.medium.com/what-is-jio-and-why-are-techs-biggest-players-suddenly-obsessed-with-it-231ea2d407e4\",\"domain\":\"onezero.medium.com\"},{\"id\":24054313,\"title\":\"What is 5D chess?\",\"points\":160,\"user\":\"searchableguy\",\"time\":1596573931,\"time_ago\":\"a day ago\",\"comments_count\":73,\"type\":\"link\",\"url\":\"https://en.chessbase.com/post/what-on-earth-is-5d-chess\",\"domain\":\"en.chessbase.com\"},{\"id\":24067748,\"title\":\"Node Modules at War: Why CommonJS and ES Modules Can’t Get Along\",\"points\":6,\"user\":\"dfabulich\",\"time\":1596682885,\"time_ago\":\"an hour ago\",\"comments_count\":1,\"type\":\"link\",\"url\":\"https://redfin.engineering/node-modules-at-war-why-commonjs-and-es-modules-cant-get-along-9617135eeca1\",\"domain\":\"redfin.engineering\"},{\"id\":24059779,\"title\":\"Ten takeaways from ten years at Retraction Watch\",\"points\":26,\"user\":\"severine\",\"time\":1596628282,\"time_ago\":\"16 hours ago\",\"comments_count\":4,\"type\":\"link\",\"url\":\"https://retractionwatch.com/2020/08/03/ten-takeaways-from-ten-years-at-retraction-watch/\",\"domain\":\"retractionwatch.com\"},{\"id\":24067007,\"title\":\"The Expansion of the Clean Network to Safeguard America’s Assets\",\"points\":72,\"user\":\"PatrolX\",\"time\":1596673101,\"time_ago\":\"4 hours ago\",\"comments_count\":43,\"type\":\"link\",\"url\":\"https://www.state.gov/announcing-the-expansion-of-the-clean-network-to-safeguard-americas-assets/\",\"domain\":\"state.gov\"},{\"id\":24060799,\"title\":\"Launch HN: Speedscale (YC S20) – Automatically create tests from actual traffic\",\"points\":106,\"user\":\"Inchull\",\"time\":1596635967,\"time_ago\":\"14 hours ago\",\"comments_count\":32,\"type\":\"link\",\"url\":\"item?id=24060799\"},{\"id\":24059282,\"title\":\"Wasabi fire alarm a lifesaver for the deaf (2008)\",\"points\":49,\"user\":\"EndXA\",\"time\":1596624559,\"time_ago\":\"17 hours ago\",\"comments_count\":19,\"type\":\"link\",\"url\":\"https://www.reuters.com/article/us-japan-wasabi-idUST29421020080318\",\"domain\":\"reuters.com\"},{\"id\":24057194,\"title\":\"The Cleopatra’s Nose of 1914\",\"points\":12,\"user\":\"benbreen\",\"time\":1596600703,\"time_ago\":\"a day ago\",\"comments_count\":1,\"type\":\"link\",\"url\":\"https://www.laphamsquarterly.org/scandal/cleopatras-nose-1914\",\"domain\":\"laphamsquarterly.org\"},{\"id\":24067367,\"title\":\"Fuzzbuzz (YC W19) Is Hiring a Sr Back End Engineer (Remote/US Only)\",\"points\":null,\"user\":null,\"time\":1596677638,\"time_ago\":\"2 hours ago\",\"comments_count\":0,\"type\":\"job\",\"url\":\"https://angel.co/company/fuzzbuzz/jobs/924765-backend-software-engineer\",\"domain\":\"angel.co\"},{\"id\":24062639,\"title\":\"Blissymbols\",\"points\":3,\"user\":\"polm23\",\"time\":1596644950,\"time_ago\":\"12 hours ago\",\"comments_count\":0,\"type\":\"link\",\"url\":\"https://en.wikipedia.org/wiki/Blissymbols\",\"domain\":\"en.wikipedia.org\"},{\"id\":24059243,\"title\":\"Moved a server from one building to another with zero downtime\",\"points\":868,\"user\":\"huhtenberg\",\"time\":1596624267,\"time_ago\":\"17 hours ago\",\"comments_count\":365,\"type\":\"link\",\"url\":\"https://www.reddit.com/r/sysadmin/comments/i3xbjb/rant_sorta_physically_moved_a_server_today/\",\"domain\":\"reddit.com\"},{\"id\":24059441,\"title\":\"Some Fundamental Theorems in Mathematics (2019) [pdf]\",\"points\":201,\"user\":\"bryanrasmussen\",\"time\":1596626000,\"time_ago\":\"17 hours ago\",\"comments_count\":84,\"type\":\"link\",\"url\":\"http://people.math.harvard.edu/~knill/graphgeometry/papers/fundamental.pdf\",\"domain\":\"people.math.harvard.edu\"},{\"id\":24053925,\"title\":\"Why did the atomic spy do it?\",\"points\":16,\"user\":\"Petiver\",\"time\":1596571284,\"time_ago\":\"a day ago\",\"comments_count\":6,\"type\":\"link\",\"url\":\"https://www.nature.com/articles/d41586-020-02279-4\",\"domain\":\"nature.com\"},{\"id\":24061164,\"title\":\"Vitamin D deficiency and Covid-19 mortality [pdf]\",\"points\":280,\"user\":\"black6\",\"time\":1596637804,\"time_ago\":\"13 hours ago\",\"comments_count\":211,\"type\":\"link\",\"url\":\"https://borsche.de/res/Vitamin_D_Essentials_EN.pdf\",\"domain\":\"borsche.de\"},{\"id\":24059356,\"title\":\"Teaching quantum information science to high-school students\",\"points\":18,\"user\":\"mathgenius\",\"time\":1596625186,\"time_ago\":\"17 hours ago\",\"comments_count\":1,\"type\":\"link\",\"url\":\"https://scirate.com/arxiv/2005.07874\",\"domain\":\"scirate.com\"}]";
		JsonParser jsonParser = new JsonParser();
		JsonArray jsonArray = (JsonArray) jsonParser.parse(json);
		HackersNewsModel news = new HackersNewsModel();
		for (int i = 0; i < jsonArray.size(); i++) {
			JsonObject object = (JsonObject) jsonArray.get(i);
			news.setId(Objects.isNull(object.get("id")) || object.get("id").isJsonNull() ? 0 : object.get("id").getAsInt());
			news.setTitle(Objects.isNull(object.get("title")) || object.get("title").isJsonNull() ? "" : object.get("title").getAsString());
			news.setPoints(Objects.isNull(object.get("points")) || object.get("points").isJsonNull() ?  0 : object.get("points").getAsInt());
			news.setName(Objects.isNull(object.get("user")) || object.get("user").isJsonNull() ? "" : object.get("user").getAsString());
			news.setTime(Objects.isNull(object.get("time")) || object.get("time").isJsonNull() ? 0 : object.get("time").getAsInt());
			news.setTime_ago(Objects.isNull(object.get("time_ago")) || object.get("time_ago").isJsonNull() ? "" : object.get("time_ago").getAsString());
			news.setComments_count(Objects.isNull(object.get("comments_count")) || object.get("comments_count").isJsonNull() ? 0 : object.get("comments_count").getAsInt());
			news.setType(Objects.isNull(object.get("type")) || object.get("type").isJsonNull() ? "" : object.get("type").getAsString());
			news.setUrl(Objects.isNull(object.get("url")) || object.get("url").isJsonNull() ? "" : object.get("url").getAsString());
			news.setDomain(Objects.isNull(object.get("domain")) || object.get("domain").isJsonObject() ? "" : object.get("domain").getAsString());
			news.setCategory(CategoryType.News);
			dao.mergeHackersNews(news);
		}
		return new ModelAndView("index");
    }
    
    @RequestMapping(value="/insertAsk", method=RequestMethod.GET)
    public ModelAndView insertAsk() {
    	String json = "[{\"id\":24066923,\"title\":\"Ask HN: 5K 27“ computer monitor recommendation?\",\"points\":17,\"user\":\"thoughtpeddler\",\"time\":1596672261,\"time_ago\":\"5 hours ago\",\"comments_count\":14,\"type\":\"ask\",\"url\":\"item?id=24066923\"},{\"id\":24067406,\"title\":\"Ask HN: Can we get a black bar in memoriam of Turing Award winner Frances Allen?\",\"points\":6,\"user\":\"relaunched\",\"time\":1596678224,\"time_ago\":\"3 hours ago\",\"comments_count\":0,\"type\":\"ask\",\"url\":\"item?id=24067406\"},{\"id\":24061778,\"title\":\"Ask HN: How to Start a Podcast in 2020?\",\"points\":7,\"user\":\"simonebrunozzi\",\"time\":1596640800,\"time_ago\":\"13 hours ago\",\"comments_count\":5,\"type\":\"ask\",\"url\":\"item?id=24061778\"},{\"id\":24050732,\"title\":\"Ask HN: What was the best project management style you have been a part of?\",\"points\":42,\"user\":\"MPiccinato\",\"time\":1596553707,\"time_ago\":\"2 days ago\",\"comments_count\":13,\"type\":\"ask\",\"url\":\"item?id=24050732\"},{\"id\":24061664,\"title\":\"Manage all your Databases from one Web-app\",\"points\":4,\"user\":\"Denislav\",\"time\":1596640222,\"time_ago\":\"13 hours ago\",\"comments_count\":9,\"type\":\"link\",\"url\":\"item?id=24061664\"},{\"id\":24054660,\"title\":\"Increasing generality in machine learning through procedural content generation\",\"points\":7,\"user\":\"togelius\",\"time\":1596576312,\"time_ago\":\"a day ago\",\"comments_count\":2,\"type\":\"link\",\"url\":\"item?id=24054660\"},{\"id\":24047007,\"title\":\"Ask HN: How do you build up your first career close to age 40?\",\"points\":37,\"user\":\"zboox\",\"time\":1596519851,\"time_ago\":\"2 days ago\",\"comments_count\":23,\"type\":\"ask\",\"url\":\"item?id=24047007\"},{\"id\":24042742,\"title\":\"Tell HN: the word \\\"Niger\\\" (the country) seems blocked in Venmo memos\",\"points\":39,\"user\":\"broahmed\",\"time\":1596485420,\"time_ago\":\"2 days ago\",\"comments_count\":17,\"type\":\"link\",\"url\":\"item?id=24042742\"},{\"id\":24059538,\"title\":\"Ask HN: Is BUPA's 'opt-out by email' legal?\",\"points\":5,\"user\":\"throwawaybupaq\",\"time\":1596626677,\"time_ago\":\"17 hours ago\",\"comments_count\":1,\"type\":\"ask\",\"url\":\"item?id=24059538\"},{\"id\":24056714,\"title\":\"New cloud storage with privacy first approach\",\"points\":4,\"user\":\"TCLOUD\",\"time\":1596594748,\"time_ago\":\"a day ago\",\"comments_count\":2,\"type\":\"link\",\"url\":\"item?id=24056714\"},{\"id\":24051365,\"title\":\"Ask HN: Are there any regulatory bodies for software developers?\",\"points\":6,\"user\":\"trwhite\",\"time\":1596557467,\"time_ago\":\"2 days ago\",\"comments_count\":8,\"type\":\"ask\",\"url\":\"item?id=24051365\"},{\"id\":24044773,\"title\":\"Ask HN: How do I find a career path?\",\"points\":14,\"user\":\"nocyno\",\"time\":1596497740,\"time_ago\":\"2 days ago\",\"comments_count\":16,\"type\":\"ask\",\"url\":\"item?id=24044773\"},{\"id\":24047514,\"title\":\"Ask HN: How to divide time code/support/marketing as a generalist?\",\"points\":5,\"user\":\"adityarao310\",\"time\":1596525371,\"time_ago\":\"2 days ago\",\"comments_count\":4,\"type\":\"ask\",\"url\":\"item?id=24047514\"},{\"id\":24046892,\"title\":\"Ask HN: What are some good resources to learn about architecture?\",\"points\":8,\"user\":\"perfectfourth\",\"time\":1596518268,\"time_ago\":\"2 days ago\",\"comments_count\":7,\"type\":\"ask\",\"url\":\"item?id=24046892\"},{\"id\":24044039,\"title\":\"Ask HN: Ambitious robotics entrepreneur with failing Kickstarter. Seeking advice\",\"points\":8,\"user\":\"pinkrobotics\",\"time\":1596492494,\"time_ago\":\"2 days ago\",\"comments_count\":12,\"type\":\"ask\",\"url\":\"item?id=24044039\"},{\"id\":24054535,\"title\":\"Ask HN: What kind of profitable business you can create with only $10K USD?\",\"points\":15,\"user\":\"designium\",\"time\":1596575497,\"time_ago\":\"a day ago\",\"comments_count\":21,\"type\":\"ask\",\"url\":\"item?id=24054535\"},{\"id\":24048129,\"title\":\"Moving toward continuous deliver/continuous integration\",\"points\":5,\"user\":\"hvgoldie\",\"time\":1596532372,\"time_ago\":\"2 days ago\",\"comments_count\":13,\"type\":\"link\",\"url\":\"item?id=24048129\"},{\"id\":24054991,\"title\":\"Ask HN: Quiethn.com is down, what's the best low-UI alternative HN reader?\",\"points\":5,\"user\":\"chr15m\",\"time\":1596578807,\"time_ago\":\"a day ago\",\"comments_count\":6,\"type\":\"ask\",\"url\":\"item?id=24054991\"},{\"id\":24046982,\"title\":\"Ask HN: Data Matching and Reconciliation machine learning algorithms suggestions\",\"points\":3,\"user\":\"maddy1512\",\"time\":1596519485,\"time_ago\":\"2 days ago\",\"comments_count\":3,\"type\":\"ask\",\"url\":\"item?id=24046982\"},{\"id\":24051491,\"title\":\"Ask HN: Should one buy Intel-based Macs now?\",\"points\":30,\"user\":\"adur1990\",\"time\":1596558245,\"time_ago\":\"2 days ago\",\"comments_count\":48,\"type\":\"ask\",\"url\":\"item?id=24051491\"},{\"id\":24046139,\"title\":\"Ask HN: How can I do professional networking without LinkedIn?\",\"points\":8,\"user\":\"passionatelycur\",\"time\":1596510470,\"time_ago\":\"2 days ago\",\"comments_count\":5,\"type\":\"ask\",\"url\":\"item?id=24046139\"},{\"id\":24047683,\"title\":\"Ask HN: Do you have a daily cash stream?\",\"points\":40,\"user\":\"wprapido\",\"time\":1596527459,\"time_ago\":\"2 days ago\",\"comments_count\":48,\"type\":\"ask\",\"url\":\"item?id=24047683\"},{\"id\":24064987,\"title\":\"Ask HN: What's the name of an AWS UI alternative?\",\"points\":1,\"user\":\"immago\",\"time\":1596658043,\"time_ago\":\"9 hours ago\",\"comments_count\":2,\"type\":\"ask\",\"url\":\"item?id=24064987\"},{\"id\":24064743,\"title\":\"Ask HN: Why so many Contact Tracing Apps?\",\"points\":1,\"user\":\"_wldu\",\"time\":1596656538,\"time_ago\":\"9 hours ago\",\"comments_count\":1,\"type\":\"ask\",\"url\":\"item?id=24064743\"},{\"id\":24063852,\"title\":\"Ask HN: Is there empirical evidence that static types reduce bugs?\",\"points\":2,\"user\":\"Kinrany\",\"time\":1596651494,\"time_ago\":\"10 hours ago\",\"comments_count\":3,\"type\":\"ask\",\"url\":\"item?id=24063852\"},{\"id\":24063842,\"title\":\"Ask HN: Learning material about storage of gaming telemetries in column storage?\",\"points\":1,\"user\":\"markus_zhang\",\"time\":1596651442,\"time_ago\":\"10 hours ago\",\"comments_count\":1,\"type\":\"ask\",\"url\":\"item?id=24063842\"},{\"id\":24063816,\"title\":\"Ask HN: How can I help make a difference?\",\"points\":3,\"user\":\"ta83728247\",\"time\":1596651325,\"time_ago\":\"10 hours ago\",\"comments_count\":1,\"type\":\"ask\",\"url\":\"item?id=24063816\"},{\"id\":24063704,\"title\":\"Ask HN: How should I start building projects with C++?\",\"points\":1,\"user\":\"nerrons\",\"time\":1596650832,\"time_ago\":\"11 hours ago\",\"comments_count\":0,\"type\":\"ask\",\"url\":\"item?id=24063704\"},{\"id\":24063676,\"title\":\"Ask HN: Would you find antipiracy telemetry intrusive?\",\"points\":1,\"user\":\"EdwinLarkin\",\"time\":1596650710,\"time_ago\":\"11 hours ago\",\"comments_count\":2,\"type\":\"ask\",\"url\":\"item?id=24063676\"},{\"id\":24063669,\"title\":\"Ask HN: A website home page generator\",\"points\":1,\"user\":\"s1mpl3\",\"time\":1596650664,\"time_ago\":\"11 hours ago\",\"comments_count\":0,\"type\":\"ask\",\"url\":\"item?id=24063669\"}]";
		JsonParser jsonParser = new JsonParser();
		JsonArray jsonArray = (JsonArray) jsonParser.parse(json);
		HackersNewsModel ask = new HackersNewsModel();
		for (int i = 0; i < jsonArray.size(); i++) {
			JsonObject object = (JsonObject) jsonArray.get(i);
			ask.setId(Objects.isNull(object.get("id")) || object.get("id").isJsonNull() ? 0 : object.get("id").getAsInt());
			ask.setTitle(Objects.isNull(object.get("title")) || object.get("title").isJsonNull() ? "" : object.get("title").getAsString());
			ask.setPoints(Objects.isNull(object.get("points")) || object.get("points").isJsonNull() ?  0 : object.get("points").getAsInt());
			ask.setName(Objects.isNull(object.get("user")) || object.get("user").isJsonNull() ? "" : object.get("user").getAsString());
			ask.setTime(Objects.isNull(object.get("time")) || object.get("time").isJsonNull() ? 0 : object.get("time").getAsInt());
			ask.setTime_ago(Objects.isNull(object.get("time_ago")) || object.get("time_ago").isJsonNull() ? "" : object.get("time_ago").getAsString());
			ask.setComments_count(Objects.isNull(object.get("comments_count")) || object.get("comments_count").isJsonNull() ? 0 : object.get("comments_count").getAsInt());
			ask.setType(Objects.isNull(object.get("type")) || object.get("type").isJsonNull() ? "" : object.get("type").getAsString());
			ask.setUrl(Objects.isNull(object.get("url")) || object.get("url").isJsonNull() ? "" : object.get("url").getAsString());
			ask.setCategory(CategoryType.Ask);
			
			dao.mergeHackersNews(ask);
		}
		return new ModelAndView("index");
    }
    
    @RequestMapping(value="/insertJobs", method=RequestMethod.GET)
    public ModelAndView insertJobs() {
    	String json = "[{\"id\":24067367,\"title\":\"Fuzzbuzz (YC W19) Is Hiring a Sr Back End Engineer (Remote/US Only)\",\"points\":null,\"user\":null,\"time\":1596677638,\"time_ago\":\"3 hours ago\",\"comments_count\":0,\"type\":\"job\",\"url\":\"https://angel.co/company/fuzzbuzz/jobs/924765-backend-software-engineer\",\"domain\":\"angel.co\"},{\"id\":24066223,\"title\":\"Volley (YC W18) is hiring engineers and designers to build Alexa games\",\"points\":null,\"user\":null,\"time\":1596665832,\"time_ago\":\"6 hours ago\",\"comments_count\":0,\"type\":\"job\",\"url\":\"https://jobs.lever.co/volleythat\",\"domain\":\"jobs.lever.co\"},{\"id\":24064030,\"title\":\"ReadMe (YC W15) is hiring a React engineer for our Slate-based markdown editor\",\"points\":null,\"user\":null,\"time\":1596652533,\"time_ago\":\"10 hours ago\",\"comments_count\":0,\"type\":\"job\",\"url\":\"https://readme.com/careers#slatereact-engineer\",\"domain\":\"readme.com\"},{\"id\":24061224,\"title\":\"Taplytics (YC W14) Is Hiring a Senior Back End Engineer in Toronto\",\"points\":null,\"user\":null,\"time\":1596638128,\"time_ago\":\"14 hours ago\",\"comments_count\":0,\"type\":\"job\",\"url\":\"https://jobs.lever.co/taplytics/e3888368-dc89-4cd7-a2da-0f9961607f16\",\"domain\":\"jobs.lever.co\"},{\"id\":24059047,\"title\":\"Lazy Lantern (YC S19) is hiring senior back-end and full-stack engineers\",\"points\":null,\"user\":null,\"time\":1596622061,\"time_ago\":\"19 hours ago\",\"comments_count\":0,\"type\":\"job\",\"url\":\"https://angel.co/company/lazylantern/jobs\",\"domain\":\"angel.co\"},{\"id\":24056412,\"title\":\"Jerry (YC S17) Is Hiring Senior Software Engineers (Boston, SF Bay Area, Toronto)\",\"points\":null,\"user\":null,\"time\":1596591306,\"time_ago\":\"a day ago\",\"comments_count\":0,\"type\":\"job\",\"url\":\"https://apply.workable.com/jerry/j/FA1F4C0876/\",\"domain\":\"apply.workable.com\"},{\"id\":24054844,\"title\":\"Repl.it is hiring front end, back end, and designers\",\"points\":null,\"user\":null,\"time\":1596577731,\"time_ago\":\"a day ago\",\"comments_count\":0,\"type\":\"job\",\"url\":\"https://repl.it/jobs\",\"domain\":\"repl.it\"},{\"id\":24053023,\"title\":\"Mux (YC W16) is hiring devs that want to help others build video (remote ok)\",\"points\":null,\"user\":null,\"time\":1596565814,\"time_ago\":\"a day ago\",\"comments_count\":0,\"type\":\"job\",\"url\":\"https://mux.com/jobs?hnj=17\",\"domain\":\"mux.com\"},{\"id\":24050427,\"title\":\"Pachyderm is hiring a docs engineer, K8s DevOps and Go Dev\",\"points\":null,\"user\":null,\"time\":1596551939,\"time_ago\":\"2 days ago\",\"comments_count\":0,\"type\":\"job\",\"url\":\"https://jobs.lever.co/pachyderm/\",\"domain\":\"jobs.lever.co\"},{\"id\":24044432,\"title\":\"Enzyme (YC S17) is hiring a DevOps Engineer to speed up FDA approval\",\"points\":null,\"user\":null,\"time\":1596495133,\"time_ago\":\"2 days ago\",\"comments_count\":0,\"type\":\"job\",\"url\":\"https://angel.co/company/enzymecorp/jobs/921769-devops-internal-tooling-engineer\",\"domain\":\"angel.co\"},{\"id\":24042289,\"title\":\"GiveCampus (YC S15) hiring Sr Engineers passionate about education\",\"points\":null,\"user\":null,\"time\":1596483222,\"time_ago\":\"2 days ago\",\"comments_count\":0,\"type\":\"job\",\"url\":\"https://jobs.lever.co/givecampus/874d7233-b7a3-488d-892e-13ef717ceab7\",\"domain\":\"jobs.lever.co\"},{\"id\":24039626,\"title\":\"OneSignal (Messaging and Push Notification APIs) is hiring a full stack engineer\",\"points\":null,\"user\":null,\"time\":1596471310,\"time_ago\":\"3 days ago\",\"comments_count\":0,\"type\":\"job\",\"url\":\"https://onesignal.com/careers/890530b3-fc07-454b-9111-ddacd65384d8\",\"domain\":\"onesignal.com\"},{\"id\":24036799,\"title\":\"EasyPost Is Hiring to Take over ECommerce\",\"points\":null,\"user\":null,\"time\":1596457460,\"time_ago\":\"3 days ago\",\"comments_count\":0,\"type\":\"job\",\"url\":\"https://www.easypost.com/careers\",\"domain\":\"easypost.com\"},{\"id\":24016738,\"title\":\"Flexport Is Hiring in North America, Europe, and Asia\",\"points\":null,\"user\":null,\"time\":1596247882,\"time_ago\":\"5 days ago\",\"comments_count\":0,\"type\":\"job\",\"url\":\"https://www.flexport.com/careers\",\"domain\":\"flexport.com\"},{\"id\":24005475,\"title\":\"Reverie Labs (YC W18) is Hiring Molecular Data Scientists to Help Cure Cancer\",\"points\":null,\"user\":null,\"time\":1596167529,\"time_ago\":\"6 days ago\",\"comments_count\":0,\"type\":\"job\",\"url\":\"https://www.reverielabs.com/careers\",\"domain\":\"reverielabs.com\"},{\"id\":24001007,\"title\":\"Tesorio is Senior Analytics Engineer and Back end Engineer – join our distributed team\",\"points\":null,\"user\":null,\"time\":1596131233,\"time_ago\":\"6 days ago\",\"comments_count\":0,\"type\":\"job\",\"url\":\"https://www.tesorio.com/careers#job-openings\",\"domain\":\"tesorio.com\"},{\"id\":23998471,\"title\":\"Substack (YC W18) is hiring to build a better business model for writing\",\"points\":null,\"user\":null,\"time\":1596118796,\"time_ago\":\"7 days ago\",\"comments_count\":0,\"type\":\"job\",\"url\":\"https://substack.com/jobs\",\"domain\":\"substack.com\"},{\"id\":23995448,\"title\":\"CareRev (YC S16) Is Looking for a SaaS Customer Success Manager\",\"points\":null,\"user\":null,\"time\":1596092754,\"time_ago\":\"7 days ago\",\"comments_count\":0,\"type\":\"job\",\"url\":\"https://grnh.se/1f9e98db3us\",\"domain\":\"grnh.se\"},{\"id\":23994334,\"title\":\"Use YC's Work at a Startup to apply for hundreds of jobs with one application\",\"points\":null,\"user\":null,\"time\":1596080012,\"time_ago\":\"7 days ago\",\"comments_count\":0,\"type\":\"job\",\"url\":\"https://www.workatastartup.com/?utm_source=hn_jobs\",\"domain\":\"workatastartup.com\"},{\"id\":23992931,\"title\":\"Sirum (YC W15 Nonprofit) is hiring developers to save medicine to save lives\",\"points\":null,\"user\":null,\"time\":1596067547,\"time_ago\":\"7 days ago\",\"comments_count\":0,\"type\":\"job\",\"url\":\"https://sirum.breezy.hr/\",\"domain\":\"sirum.breezy.hr\"},{\"id\":23991429,\"title\":\"Caper (YC W16) Is Hiring Engineers – SW/CV/Android (Remote OK)\",\"points\":null,\"user\":null,\"time\":1596055636,\"time_ago\":\"7 days ago\",\"comments_count\":0,\"type\":\"job\",\"url\":\"https://recruiterflow.com/caper/jobs#menu\",\"domain\":\"recruiterflow.com\"},{\"id\":23973632,\"title\":\"Lowkey (YC S18) is hiring full-stack engineers passionate about gaming (remote ok)\",\"points\":null,\"user\":null,\"time\":1595925066,\"time_ago\":\"9 days ago\",\"comments_count\":0,\"type\":\"job\",\"url\":\"https://lowkey.gg\",\"domain\":\"lowkey.gg\"},{\"id\":23972493,\"title\":\"BuildZoom (better way to build custom homes) is hiring in Scottsdale\",\"points\":null,\"user\":null,\"time\":1595913154,\"time_ago\":\"9 days ago\",\"comments_count\":0,\"type\":\"job\",\"url\":\"https://jobs.lever.co/buildzoom\",\"domain\":\"jobs.lever.co\"},{\"id\":23971527,\"title\":\"Draftbit (YC W18) is hiring a Product Manager to build the future of no-code\",\"points\":null,\"user\":null,\"time\":1595901242,\"time_ago\":\"9 days ago\",\"comments_count\":0,\"type\":\"job\",\"url\":\"https://draftbit.com/jobs/product-manager\",\"domain\":\"draftbit.com\"},{\"id\":23970217,\"title\":\"Make School is recruiting an adjunct faculty to teach at an HBCU\",\"points\":null,\"user\":null,\"time\":1595889330,\"time_ago\":\"9 days ago\",\"comments_count\":0,\"type\":\"job\",\"url\":\"https://docs.google.com/document/d/1UgK8Lb8vQLPijElrQEl2t0bZqwu28xBdQRKAbjLj21Y/preview\",\"domain\":\"docs.google.com\"},{\"id\":23968175,\"title\":\"Nimble (YC S17) is hiring full-stack dev (React/Django)\",\"points\":null,\"user\":null,\"time\":1595876034,\"time_ago\":\"9 days ago\",\"comments_count\":0,\"type\":\"job\",\"url\":\"item?id=23968175\"},{\"id\":23966089,\"title\":\"BackerKit (YC S12) is hiring our second Product Manager\",\"points\":null,\"user\":null,\"time\":1595864121,\"time_ago\":\"10 days ago\",\"comments_count\":0,\"type\":\"job\",\"url\":\"https://jobs.lever.co/backerkit/78009a7b-34ba-46f3-ac96-34ba7e28789a\",\"domain\":\"jobs.lever.co\"},{\"id\":23931643,\"title\":\"Soteris (YC S19), ML to price insurance, is hiring employees #3-5\",\"points\":null,\"user\":null,\"time\":1595534534,\"time_ago\":\"13 days ago\",\"comments_count\":0,\"type\":\"job\",\"url\":\"https://www.soteris.co/#careers\",\"domain\":\"soteris.co\"},{\"id\":23925759,\"title\":\"Modal (YC W16) Is Hiring a Principal Architect\",\"points\":null,\"user\":null,\"time\":1595498580,\"time_ago\":\"14 days ago\",\"comments_count\":0,\"type\":\"job\",\"url\":\"https://jobs.lever.co/modal/3da3afed-0701-4905-8f2c-ceb817411b19\",\"domain\":\"jobs.lever.co\"},{\"id\":23923740,\"title\":\"XIX (YC W17) Is Hiring Engineers and Sales Reps in San Francisco\",\"points\":null,\"user\":null,\"time\":1595472818,\"time_ago\":\"14 days ago\",\"comments_count\":0,\"type\":\"job\",\"url\":\"https://jobs.lever.co/xix\",\"domain\":\"jobs.lever.co\"}]";
		JsonParser jsonParser = new JsonParser();
		JsonArray jsonArray = (JsonArray) jsonParser.parse(json);
		HackersNewsModel jobs = new HackersNewsModel();
		for (int i = 0; i < jsonArray.size(); i++) {
			JsonObject object = (JsonObject) jsonArray.get(i);
			jobs.setId(Objects.isNull(object.get("id")) || object.get("id").isJsonNull() ? 0 : object.get("id").getAsInt());
			jobs.setTitle(Objects.isNull(object.get("title")) || object.get("title").isJsonNull() ? "" : object.get("title").getAsString());
			jobs.setPoints(Objects.isNull(object.get("points")) || object.get("points").isJsonNull() ?  0 : object.get("points").getAsInt());
			jobs.setName(Objects.isNull(object.get("user")) || object.get("user").isJsonNull() ? "" : object.get("user").getAsString());
			jobs.setTime(Objects.isNull(object.get("time")) || object.get("time").isJsonNull() ? 0 : object.get("time").getAsInt());
			jobs.setTime_ago(Objects.isNull(object.get("time_ago")) || object.get("time_ago").isJsonNull() ? "" : object.get("time_ago").getAsString());
			jobs.setComments_count(Objects.isNull(object.get("comments_count")) || object.get("comments_count").isJsonNull() ? 0 : object.get("comments_count").getAsInt());
			jobs.setType(Objects.isNull(object.get("type")) || object.get("type").isJsonNull() ? "" : object.get("type").getAsString());
			jobs.setUrl(Objects.isNull(object.get("url")) || object.get("url").isJsonNull() ? "" : object.get("url").getAsString());
			jobs.setDomain(Objects.isNull(object.get("domain")) || object.get("domain").isJsonObject() ? "" : object.get("domain").getAsString());
			jobs.setCategory(CategoryType.Jobs);
			
			dao.mergeHackersNews(jobs);
		}
		return new ModelAndView("index");
    }
    
    @RequestMapping(value="/mergeUser", method=RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> mergeUser(UserModel model) {
    	
    	try {
    		dao.mergeUser(model);
    		
    		Map<String, Object> map = new HashMap<String, Object>();
        	map.put("success", Boolean.TRUE);
        	
        	return map;
    	} catch(Exception e) {
    		throw new RuntimeException(e);
    	}
    }
    
    @RequestMapping(value="/mergeItem", method=RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> mergeItem(HackersNewsModel model) {
    	
    	try {
    		dao.mergeItem(model);
    		
    		Map<String, Object> map = new HashMap<String, Object>();
        	map.put("success", Boolean.TRUE);
        	
        	return map;
    	} catch(Exception e) {
    		throw new RuntimeException(e);
    	}
    }
    
    @RequestMapping(value="/selectUser", method=RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> selectUser(UserModel model) {
    	
    	try {
    		Map<String, Object> map = new HashMap<String, Object>();
        	Gson gson = new Gson();
        	UserModel user = dao.selectUser(model);
        	map.put("d", gson.toJson(user));
        	
        	return map;
    	} catch(Exception e) {
    		throw new RuntimeException(e);
    	}
    	
    }
    
    @RequestMapping(value="/selectItem", method=RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> selectItem(HackersNewsModel model) {
    	
    	try {
    		Map<String, Object> map = new HashMap<String, Object>();
        	Gson gson = new Gson();
        	HackersNewsModel item = dao.selectItem(model);
        	map.put("d", gson.toJson(item));
        	
        	return map;
    	} catch(Exception e) {
    		throw new RuntimeException(e);
    	}
    	
    }
    
}