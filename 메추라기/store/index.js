import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import { selectListUrl, selectUserUrl, selectItemUrl, categoriesUrl, mergeUserUrl, mergeItemUrl } from '../api/index.js';

export default new Vuex.Store({
	state : {
		totalRows : 0,
		list : [],
		user : {
			about : '',
			created_time : '',
			created : '',
			id : '',
			karma : '',
		},
		item : {
			id : '',
			title : '',
			points : '',
			name : '',
			time : '',
			time_ago : '',
			comments_count : '',
			type : '',
			url : '',
			domain : '',
			category : '01',
		},
		categories : [],
	},
	mutations : {
		SET_TOTAL_ROWS(state, totalRows) {
			state.totalRows = totalRows;
		},
		SET_LIST(state, list) {
			state.list = list;
		},
		SET_USER(state, user) {
			state.user = user;
		},
		SET_ITEM(state, item) {
			state.item = item;
		},
		SET_CATEGORIES(state, categories) {
			state.categories = categories;
		},
	},
	actions : {
		async FETCH_LIST(context, params) {
			const response = await selectListUrl(params);
			
			const totalRows = response.data.c;
			context.commit('SET_TOTAL_ROWS', totalRows);
			const list = JSON.parse(response.data.d);
			context.commit('SET_LIST', list);
			return list;
		},
		async FETCH_USER(context, params) {
			const response = await selectUserUrl(params);
			
			const user = JSON.parse(response.data.d);
			context.commit('SET_USER', user);
			return user;
		},
		async FETCH_ITEM(context, params) {
			const response = await selectItemUrl(params);
			
			const item = JSON.parse(response.data.d);
			context.commit('SET_ITEM', item);
			return item;
		},
		async FETCH_CATEGORIES(context) {
			const response = await categoriesUrl();
			const codeList = JSON.parse(response.data.d);
			let categories = [];
			codeList.forEach(v => {
				let map = new Map();
				map.set('text', v.NAME);
				map.set('value', v.CODE);
				const jsonObj = Object.fromEntries(map);
				categories.push(jsonObj);
			});
			context.commit('SET_CATEGORIES', categories);
		},
		async MERGE_USER(context, params) {
			const response = await mergeUserUrl(params);
			return response.data.success;
		},
		async MERGE_ITEM(context, params) {
			const response = await mergeItemUrl(params);
			return response.data.success;
		},
	},
	getters : {
		getTotalRows(state) {
			return state.totalRows;
		},
		getList(state) {
			return state.list;
		},
		getUser(state) {
			return state.user;
		},
		getItem(state) {
			return state.item;
		},
		getCategories(state) {
			return state.categories;
		},
	},
});