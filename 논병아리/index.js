import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import { selectListUrl, selectUserUrl, selectHackersNewsUrl, selectItemUrl, categoriesUrl, mergeUserUrl, mergeHackersNewsUrl, mergeItemUrl } from '../api/index.js';

export default new Vuex.Store({
	state : {
		totalRows : 0,
		list : [],
		blockUI : false,
		form : {
			hackersNews : {
				id : '',
				title : '',
				points : '',
				name : '',
				time : '',
				time_ago : '',
				comments_count : '',
				type : '',
				url : '',
				domain : '',
				category : '',
			},
			item : {
				content : '',
			},
			user : {
				about : '',
				created_time : '',
				created : '',
				id : '',
				karma : '',
			},
			categories : [],
		},
	},
	mutations : {
		SET_TOTAL_ROWS(state, totalRows) {
			state.totalRows = totalRows;
		},
		SET_LIST(state, list) {
			state.list = list;
		},
		SET_USER(state, user) {
			state.form.user = user;
		},
		SET_HACKERSNEWS(state, hackersNews) {
			state.form.hackersNews = hackersNews;
		},
		SET_ITEM(state, item) {
			state.form.item = item;
		},
		SET_CATEGORIES(state, categories) {
			state.form.categories = categories;
		},
		SET_BLOCKUI(state, blockUI) {
			state.blockUI = blockUI;
		},
	},
	actions : {
		async FETCH_LIST(context, params) {
			const response = await selectListUrl(params);
			
			const totalRows = response.data.c;
			context.commit('SET_TOTAL_ROWS', totalRows);
			const list = JSON.parse(response.data.d);
			context.commit('SET_LIST', list);
			return list;
		},
		async FETCH_USER(context, params) {
			const response = await selectUserUrl(params);
			
			const user = JSON.parse(response.data.d);
			context.commit('SET_USER', user);
			return response;
		},
		async FETCH_HACKERSNEWS(context, params) {
			const response = await selectHackersNewsUrl(params);
			
			const hackersNews = JSON.parse(response.data.d);
			context.commit('SET_HACKERSNEWS', hackersNews);
			return response;
		},
		async FETCH_ITEM(context, params) {
			const response = await selectItemUrl(params);
			
			const item = JSON.parse(response.data.d);
			context.commit('SET_ITEM', item);
			return response;
		},
		async FETCH_CATEGORIES(context) {
			const response = await categoriesUrl();
			const codeList = JSON.parse(response.data.d);
			let categories = [];
			codeList.forEach(v => {
				let map = new Map();
				map.set('text', v.NAME);
				map.set('value', v.CODE);
				const jsonObj = Object.fromEntries(map);
				categories.push(jsonObj);
			});
			context.commit('SET_CATEGORIES', categories);
			return response;
		},
		async MERGE_USER(context, params) {
			const response = await mergeUserUrl(params);
			return response;
		},
		async MERGE_HACKERSNEWS(context, params) {
			const response = await mergeHackersNewsUrl(params);
			return response.data.success;
		},
		async MERGE_ITEM(context, params) {
			const response = await mergeItemUrl(params);
			return response.data.success;
		},
	},
	getters : {
		getTotalRows(state) {
			return state.totalRows;
		},
		getList(state) {
			return state.list;
		},
		getUser(state) {
			return state.form.user;
		},
		getHackersNews(state) {
			return state.form.hackersNews;
		},
		getItem(state) {
			return state.form.item;
		},
		getCategories(state) {
			return state.form.categories;
		},
		getBlockUI(state) {
			return state.blockUI;
		},
	},
});