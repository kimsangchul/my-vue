import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import TopComponent from '../components/TopComponent.vue';
import LeftComponent from '../components/LeftComponent.vue';
import CenterComponent from '../components/CenterComponent.vue';
import RightComponent from '../components/RightComponent.vue';
import BottomComponent from '../components/BottomComponent.vue';

import ChangeLeftComponent from '../components/ChangeLeftComponent.vue';

export default new VueRouter({
	mode : 'history',
	routes : [
		{
			path : '/admin/product',
			components : {
				top : TopComponent,
				left : LeftComponent,
				center : CenterComponent,
				right : RightComponent,
				bottom : BottomComponent,
			}
		}
	],
});