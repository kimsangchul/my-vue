import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import DetailView from '../../view/noticebar/DetailView.vue';

export default new VueRouter({
	mode : 'history',
	routes : [
		{
			name : 'noticedetail',
			path : '/admin/noticedetail',
			component : DetailView,
		}
	]
});