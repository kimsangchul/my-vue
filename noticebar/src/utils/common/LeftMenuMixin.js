import { getIaListUrl, getSiteCodeListUrl } from '../../api/noticebar/index.js';
import bus from '../../utils/noticebar/bus.js';
import store from '../../store/index.js';

export default {
	data() {
		return {
			item : {
				siteCodeList : [],
				typeList : [],
				shown : false,
				languageList : [],
				iaCode : '00000000',
				iaTypeCode : '',
				iaSubTypeCode : '',
				iaList : [],
				iaTypeList : [],
				iaSubTypeList : [],
				keywordTypeList : [],
				keywordTitle : '',
				depthLevel : 1,
			},
		}
	},
	created() {
		this.initSiteCodeList(getSiteCodeListUrl);
		this.initTypeList([
			{text : 'All', value : ''},
			{text : 'B2C', value : '1'},
			{text : 'B2B', value : '2'},
		]);
		this.initLanguageList([
			{text : 'Local', value : 'L'},
			{text : 'English', value : 'E'},
		]);
		this.initKeywordTypeList([
			{text : 'All', value : ''},
			{text : 'Open Headline', value : '1'},
			{text : 'Close Desktop Headline', value : '2'},
			{text : 'Open Description', value : '3'},
		]);
		bus.$on('initIaList', (data) => {
			getIaListUrl(data)
				.then(response => {
					// 최초 로딩 시, Site & Type & Language 변경 시
					if(data.level === 1) {
						this.item.iaCode = '00000000';
						this.item.iaTypeCode = '';
						this.item.iaSubTypeCode = '';
						this.item.iaList = JSON.parse(response.data.d);
						this.item.iaTypeList = '';
						this.item.iaSubTypeList = '';
					} 
					// Group 선택 시
					else if(data.level === 2) {
						this.item.iaCode = data.iaCode;
						this.item.iaTypeList = JSON.parse(response.data.d);
					} 
					// Type 선택 시
					else if(data.level === 3) {
						this.item.iaTypeCode = data.iaCode;
						this.item.iaSubTypeList = JSON.parse(response.data.d);
					}
					// Sub Type 선택 시
					else if(data.level === 4) {
						this.item.iaSubTypeCode = data.iaCode;
					}
				})
				.catch(error => console.log(error));
		});
	},
	methods : {
		initSiteCodeList(getSiteCodeListUrl) {
			getSiteCodeListUrl()
				.then(response => {
					this.item.siteCodeList = JSON.parse(response.data.d);
				})
				.catch(error => console.log(error));
		},
		initTypeList(typeList) {
			this.item.typeList = typeList;
		},
		initLanguageList(languageList) {
			this.item.languageList = languageList;
		},
		initKeywordTypeList(keywordTypeList) {
			this.item.keywordTypeList = keywordTypeList;
		},
		changeSiteCode(value) {
			store.commit('leftMenuStore/SET_SITECODE', value);
		},
		changeType(value, siteCode, language) {
			this.item.shown = false;
			if(value) {
				this.item.shown = true;
			}
			store.commit('noticeBarStore/SET_TYPE', value);
			const data = {
				siteCode,
				iaCode : '00000000',
				level : 1,
				iaTypeCode : value === '1' ? 'IA002' : 'IA003',
				language,
			};
			this.initIaList(data);
		},
		changeLanguage(value, siteCode, type) {
			store.commit('leftMenuStore/SET_LANGUAGE', value);
			const data = {
				siteCode,
				iaCode : '00000000',
				level : 1,
				iaTypeCode : type === '1' ? 'IA002' : 'IA003',
				language : value,
			};
			this.initIaList(data);
		},
	}
}