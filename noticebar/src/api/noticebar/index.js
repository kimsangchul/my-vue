import axios from 'axios';

const context = {
	root : '/admin',
}

function getSiteCodeListUrl() {
	return axios.get(`${context.root}/getSiteCodeList`);
}

function getIaListUrl(data) {
	return axios.get(`${context.root}/getIaList`, {
	    params : data,
	});
}

function getNoticeBarListUrl(data) {
	return axios.get(`${context.root}/selectNoticeBarList`, {
	    params : data,
	});
}

function getNoticeBarUrl(data) {
	return axios.get(`${context.root}/selectNoticeBar`, {
	    params : data,
	});
}

export {
	getSiteCodeListUrl,
	getIaListUrl,
	getNoticeBarListUrl,
	getNoticeBarUrl,
}