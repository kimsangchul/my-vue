const leftMenuStore = {
	namespaced: true,
	state : {
		slidePosition : {
			'marginLeft' : '350px',
			'marginTop' : '20px',
		},
		siteCode : 'cn',
		language : 'L',
		searchIaCode : '',
	},
	mutations : {
		SET_SLIDEPOSITION(state, slidePosition) {
			state.slidePosition = slidePosition;
		},
		SET_SITECODE(state, siteCode) {
			state.siteCode = siteCode;
		},
		SET_LANGUAGE(state, language) {
			state.language = language;
		},
		SET_SEARCHIACODE(state, searchIaCode) {
			state.searchIaCode = searchIaCode;
		},
	},
	actions : {
		
	},
	getters : {
		getSlidePosition(state) {
			return state.slidePosition;
		},
		getSiteCode(state) {
			return state.siteCode;
		},
		getLanguage(state) {
			return state.language;
		},
		getSearchIaCode(state) {
			return state.searchIaCode;
		},
	},
}

export default leftMenuStore;