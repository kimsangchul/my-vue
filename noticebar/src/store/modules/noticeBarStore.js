import { getNoticeBarListUrl, getNoticeBarUrl } from '../../api/noticebar/index.js';

const noticeBarStore = {
	namespaced: true,
	state : {
		type : '1',
		noticeId : '',
		keywordType : '',
		keywordTitle : '',
		noticeList : [],
		fields : [
			{ key: 'noticeTitle', label: 'Open Headline', sortable: true, class: 'text-center' },
			{ key: 'noticeTitleDesktopClose', label: 'Close Desktop Headline', sortable: true, class: 'text-center' },
			{ key: 'noticeText', label: 'Open Description', sortable: true, class: 'text-center' },
			{ key: 'noticeId', label: 'Notice ID', sortable: true, class: 'text-center' },
			{ key: 'b2cTypeBv', label: 'Type', sortable: true, class: 'text-center' },
			{ key: 'noticeUseFlag', label: 'Use', sortable: true, class: 'text-center' },
			{ key: 'udtDate', label: 'Update Date', sortable: true, class: 'text-center' },
			{ key: 'udtUserId', label: 'Update User', sortable: true, class: 'text-center' },
		],
		table : {
			sortBy : 'NOTICE_ID',
			sortDesc : 'DESC',
			currentPage : 1,
			perPage : 10,
		},
		keywordTypeList : [],
		notice : {},
	},
	mutations : {
		SET_TYPE(state, type) {
			state.type = type;
		},
		SET_NOTICEID(state, noticeId) {
			state.noticeId = noticeId;
		},
		SET_KEYWORDTYPE(state, keywordType) {
			state.keywordType = keywordType;
		},
		SET_KEYWORDTITLE(state, keywordTitle) {
			state.keywordTitle = keywordTitle;
		},
		SET_NOTICELIST(state, noticeList) {
			state.noticeList = noticeList;
		},
		SET_KEYWORDTYPELIST(state, keywordTypeList) {
			state.keywordTypeList = keywordTypeList;
		},
		SET_CURRENTPAGE(state, currentPage) {
			state.table.currentPage = currentPage;
		},
		SET_NOTICE(state, notice) {
			state.notice = notice;
		},
	},
	actions : {
		FETCH_TABLE({commit}, data) {
			return getNoticeBarListUrl(data)
				.then(response => {
					commit('SET_NOTICELIST', JSON.parse(response.data.d));
					return response;
				})
				.catch(error => console.log(error));
		},
		FETCH_NOTICE({commit}, data) {
			return getNoticeBarUrl(data)
				.then(response => {
					commit('SET_NOTICE', JSON.parse(response.data.d));
					return response;
				})
				.catch(error => console.log(error));
		},
	},
	getters : {
		getType(state) {
			return state.type;
		},
		getNoticeList(state) {
			return state.noticeList;
		},
		getFields(state) {
			return state.fields;
		},
		getSortBy(state) {
			return state.table.sortBy;
		},
		getSortDesc(state) {
			return state.table.sortDesc;
		},
		getKeywordTypeList(state) {
			return state.keywordTypeList;
		},
		getCurrentPage(state) {
			return state.table.currentPage;
		},
		getPerPage(state) {
			return state.table.perPage;
		},
		getNoticeId(state) {
			return state.noticeId;
		},
		getKeywordType(state) {
			return state.keywordType;
		},
		getKeywordTitle(state) {
			return state.keywordTitle;
		},
		getNotice(state) {
			return state.notice;
		},
	},
}

export default noticeBarStore;