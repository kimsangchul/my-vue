import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import leftMenuStore from './modules/leftMenuStore.js';
import noticeBarStore from './modules/noticeBarStore.js';

export default new Vuex.Store({
	modules : {
		leftMenuStore,
		noticeBarStore,
	}
});