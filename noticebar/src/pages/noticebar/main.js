import Vue from 'vue'
import App from './App.vue'

import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.config.productionTip = false

/*
import '@/css/common-clientlibs/css/bootstrap-wcms.css';
import '@/css/style-pim.css';
import '@/css/slidebars.css';
*/

import store from '../../store/index.js';
import router from '../../routes/noticebar/index.js';

new Vue({
  render: h => h(App),
  store,
  router,
}).$mount('#app')
