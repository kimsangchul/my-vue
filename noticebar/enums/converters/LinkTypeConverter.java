/**
 * Copyright (c) 2011 Samsung Electronics. All Rights Reserved.
 * Project: Samsung.com Site Renewal(2011.03~08)
 */
package com.sec.pim.enums.converters;

import org.springframework.core.convert.converter.Converter;

import com.sec.pim.enums.LinkType;

public class LinkTypeConverter implements Converter<String, LinkType> {

	@Override
	public LinkType convert(String arg0) {

		for (LinkType item : LinkType.values()) {
			if (item.getCode().equals(arg0)) {
				return item;
			}
		}
		return null;

	}

}