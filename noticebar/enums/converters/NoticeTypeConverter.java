/**
 * Copyright (c) 2011 Samsung Electronics. All Rights Reserved.
 * Project: Samsung.com Site Renewal(2011.03~08)
 */
package com.sec.pim.enums.converters;

import org.springframework.core.convert.converter.Converter;

import com.sec.pim.enums.NoticeType;

public class NoticeTypeConverter implements Converter<String, NoticeType> {

	@Override
	public NoticeType convert(String arg0) {

		for (NoticeType item : NoticeType.values()) {
			if (item.getCode().equals(arg0)) {
				return item;
			}
		}
		return null;

	}

}