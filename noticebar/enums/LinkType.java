package com.sec.pim.enums;

import org.apache.ibatis.type.MappedTypes;
import org.codehaus.jackson.annotate.JsonValue;

public enum LinkType implements CodeEnum {

	MOVE("MOVE", "Move"), 
	NEW("NEW", "New"),
	POPUP("POPUP", "Popup");

	private String code;
	private String name;

	LinkType(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@MappedTypes(LinkType.class)
	public static class TypeHandler extends CodeEnumTypeHandler<LinkType> {
		public TypeHandler() {
			super(LinkType.class);
		}
	}

	@Override
	@JsonValue
	public String getCode() {
		return code;
	}
}