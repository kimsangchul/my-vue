package com.sec.pim.enums;

public interface CodeEnum {
	String getCode();
}