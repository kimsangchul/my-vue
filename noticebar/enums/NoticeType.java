package com.sec.pim.enums;

import org.apache.ibatis.type.MappedTypes;
import org.codehaus.jackson.annotate.JsonValue;

public enum NoticeType implements CodeEnum {

	NOTICE_BAR("NOTICE-BAR", "Notice Bar");

	private String code;
	private String name;

	NoticeType(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@MappedTypes(NoticeType.class)
	public static class TypeHandler extends CodeEnumTypeHandler<NoticeType> {
		public TypeHandler() {
			super(NoticeType.class);
		}
	}

	@Override
	@JsonValue
	public String getCode() {
		return code;
	}
}