package com.sec.pim.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sec.pim.model.CategoryModel;
import com.sec.pim.model.NoticeBarModel;
import com.sec.pim.model.SiteModel;

@Repository
public class NoticeBarDao {
	
	@Autowired
    private SqlSessionTemplate sqlSession;

	public List<SiteModel> getSiteCodeList() {
		return sqlSession.selectList("noticebar.getSiteCodeList");
	}
	
	public List<CategoryModel> getIaList(CategoryModel categoryModel) {
		return sqlSession.selectList("noticebar.getIaList", categoryModel);
	}

	public List<NoticeBarModel> selectNoticeBarList(NoticeBarModel noticeBarModel) {
		return sqlSession.selectList("noticebar.selectNoticeBarList", noticeBarModel);
	}

	public NoticeBarModel selectNoticeBar(NoticeBarModel noticeBarModel) {
		return sqlSession.selectOne("noticebar.selectNoticeBar", noticeBarModel);
	}
	
}
