package com.sec.pim.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.sec.pim.dao.NoticeBarDao;
import com.sec.pim.model.CategoryModel;
import com.sec.pim.model.NoticeBarModel;
import com.sec.pim.model.SiteModel;

@Controller
public class NoticeBarController {
	
	@Autowired
	private NoticeBarDao dao;
	
    @RequestMapping(value="/index", method=RequestMethod.GET)
    public ModelAndView index() {
    	return new ModelAndView("index");
    }
    
    @RequestMapping(value="/noticedetail", method=RequestMethod.GET)
    public ModelAndView noticedetail() {
    	return new ModelAndView("index");
    }
    
    @RequestMapping(value="/getSiteCodeList", method=RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getSiteCodeList() {
    	
    	try {
    		Map<String, Object> resultMap = new HashMap<String, Object>();
        	Gson gson = new Gson();
        	List<SiteModel> iaList = dao.getSiteCodeList();
        	resultMap.put("d", gson.toJson(iaList));
        	
        	return resultMap;
    	} catch(Exception e) {
    		throw new RuntimeException(e);
    	}
    	
    }
    
    @RequestMapping(value="/getIaList", method=RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getIaList(CategoryModel categoryModel) {
    	
    	try {
    		Map<String, Object> resultMap = new HashMap<String, Object>();
        	Gson gson = new Gson();
        	List<CategoryModel> iaList = dao.getIaList(categoryModel);
        	resultMap.put("d", gson.toJson(iaList));
        	
        	return resultMap;
    	} catch(Exception e) {
    		throw new RuntimeException(e);
    	}
    	
    }
    
    @RequestMapping(value="/selectNoticeBarList", method=RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> selectNoticeBarList(NoticeBarModel noticeBarModel) {
    	
    	try {
    		Map<String, Object> map = new HashMap<String, Object>();
        	Gson gson = new Gson();
        	List<NoticeBarModel> noticeBarList = dao.selectNoticeBarList(noticeBarModel);
        	map.put("c", 0);
        	if(noticeBarList.stream().findFirst().map(e -> e.getRecords()).isPresent()) {
        		map.put("c", noticeBarList.stream().findFirst().map(e -> e.getRecords()).get());
        	}
        	map.put("d", gson.toJson(noticeBarList));
        	
        	return map;
    	} catch(Exception e) {
    		throw new RuntimeException(e);
    	}
    	
    }
    
    @RequestMapping(value="/selectNoticeBar", method=RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> selectNoticeBar(NoticeBarModel noticeBarModel) {
    	
    	try {
    		Map<String, Object> map = new HashMap<String, Object>();
        	Gson gson = new Gson();
        	NoticeBarModel noticeBar = dao.selectNoticeBar(noticeBarModel);
        	map.put("d", gson.toJson(noticeBar));
        	
        	return map;
    	} catch(Exception e) {
    		throw new RuntimeException(e);
    	}
    	
    }
    
}