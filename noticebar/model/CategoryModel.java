package com.sec.pim.model;

public class CategoryModel {
	
	private String siteCode;
	
	private String iaTypeCode;
	
	private String iaCode;
	
	private String parentIaCode;
	
	private String iaEnglishName;
	
	private String iaName;
	
	private String language;
	
	private int level;

	public String getSiteCode() {
		return siteCode;
	}

	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}

	public String getIaTypeCode() {
		return iaTypeCode;
	}

	public void setIaTypeCode(String iaTypeCode) {
		this.iaTypeCode = iaTypeCode;
	}

	public String getIaCode() {
		return iaCode;
	}

	public void setIaCode(String iaCode) {
		this.iaCode = iaCode;
	}

	public String getParentIaCode() {
		return parentIaCode;
	}

	public void setParentIaCode(String parentIaCode) {
		this.parentIaCode = parentIaCode;
	}

	public String getIaEnglishName() {
		return iaEnglishName;
	}

	public void setIaEnglishName(String iaEnglishName) {
		this.iaEnglishName = iaEnglishName;
	}

	public String getIaName() {
		return iaName;
	}

	public void setIaName(String iaName) {
		this.iaName = iaName;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}
	
}
