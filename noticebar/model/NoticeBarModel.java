package com.sec.pim.model;

import com.sec.pim.enums.LinkType;
import com.sec.pim.enums.NoticeType;

public class NoticeBarModel extends SearchModel {
	
	private String siteCode;
	
	private String noticeId;
	
	private NoticeType noticeTypeCode;
	
	private String noticeTitle;
	
	private String noticeUseFlag;
	
	private String noticeText;
	
	private String ctaTitle;
	
	private String ctaText;
	
	private String ctaEnglishText;
	
	private String ctaUrl;
	
	private LinkType linkTypeCode;
	
	private String regDate;
	
	private String regUserId;
	
	private String udtDate;
	
	private String udtUserId;
	
	private String syncDate;
	
	private String b2cTypeBv;
	
	private String noticeTitleDesktopClose;
	
	private String noticeTitleMobileClose;

	public String getSiteCode() {
		return siteCode;
	}

	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}

	public String getNoticeId() {
		return noticeId;
	}

	public void setNoticeId(String noticeId) {
		this.noticeId = noticeId;
	}

	public NoticeType getNoticeTypeCode() {
		return noticeTypeCode;
	}

	public void setNoticeTypeCode(NoticeType noticeTypeCode) {
		this.noticeTypeCode = noticeTypeCode;
	}

	public String getNoticeTitle() {
		return noticeTitle;
	}

	public void setNoticeTitle(String noticeTitle) {
		this.noticeTitle = noticeTitle;
	}

	public String getNoticeUseFlag() {
		return noticeUseFlag;
	}

	public void setNoticeUseFlag(String noticeUseFlag) {
		this.noticeUseFlag = noticeUseFlag;
	}

	public String getNoticeText() {
		return noticeText;
	}

	public void setNoticeText(String noticeText) {
		this.noticeText = noticeText;
	}

	public String getCtaTitle() {
		return ctaTitle;
	}

	public void setCtaTitle(String ctaTitle) {
		this.ctaTitle = ctaTitle;
	}

	public String getCtaText() {
		return ctaText;
	}

	public void setCtaText(String ctaText) {
		this.ctaText = ctaText;
	}

	public String getCtaEnglishText() {
		return ctaEnglishText;
	}

	public void setCtaEnglishText(String ctaEnglishText) {
		this.ctaEnglishText = ctaEnglishText;
	}

	public String getCtaUrl() {
		return ctaUrl;
	}

	public void setCtaUrl(String ctaUrl) {
		this.ctaUrl = ctaUrl;
	}

	public LinkType getLinkTypeCode() {
		return linkTypeCode;
	}

	public void setLinkTypeCode(LinkType linkTypeCode) {
		this.linkTypeCode = linkTypeCode;
	}

	public String getRegDate() {
		return regDate;
	}

	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}

	public String getRegUserId() {
		return regUserId;
	}

	public void setRegUserId(String regUserId) {
		this.regUserId = regUserId;
	}

	public String getUdtDate() {
		return udtDate;
	}

	public void setUdtDate(String udtDate) {
		this.udtDate = udtDate;
	}

	public String getUdtUserId() {
		return udtUserId;
	}

	public void setUdtUserId(String udtUserId) {
		this.udtUserId = udtUserId;
	}

	public String getSyncDate() {
		return syncDate;
	}

	public void setSyncDate(String syncDate) {
		this.syncDate = syncDate;
	}

	public String getB2cTypeBv() {
		return b2cTypeBv;
	}

	public void setB2cTypeBv(String b2cTypeBv) {
		this.b2cTypeBv = b2cTypeBv;
	}

	public String getNoticeTitleDesktopClose() {
		return noticeTitleDesktopClose;
	}

	public void setNoticeTitleDesktopClose(String noticeTitleDesktopClose) {
		this.noticeTitleDesktopClose = noticeTitleDesktopClose;
	}

	public String getNoticeTitleMobileClose() {
		return noticeTitleMobileClose;
	}

	public void setNoticeTitleMobileClose(String noticeTitleMobileClose) {
		this.noticeTitleMobileClose = noticeTitleMobileClose;
	}
	
}
