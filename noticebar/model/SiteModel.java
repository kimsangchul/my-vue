package com.sec.pim.model;

public class SiteModel {
	
	private String siteCode;
	
	private String siteEnglishName;

	public String getSiteCode() {
		return siteCode;
	}

	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}

	public String getSiteEnglishName() {
		return siteEnglishName;
	}

	public void setSiteEnglishName(String siteEnglishName) {
		this.siteEnglishName = siteEnglishName;
	}
	
}
