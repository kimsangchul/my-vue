package com.sec.pim.model;

public class SearchModel extends PageModel {
	
	private String searchSiteCode;
	
	private String searchCategoryType;
	
	private String searchIaCode;
	
	private String searchNoticeId;
	
	private String searchKeywordType;
	
	private String searchKeywordTitle;

	public String getSearchSiteCode() {
		return searchSiteCode;
	}

	public void setSearchSiteCode(String searchSiteCode) {
		this.searchSiteCode = searchSiteCode;
	}

	public String getSearchCategoryType() {
		return searchCategoryType;
	}

	public void setSearchCategoryType(String searchCategoryType) {
		this.searchCategoryType = searchCategoryType;
	}

	public String getSearchIaCode() {
		return searchIaCode;
	}

	public void setSearchIaCode(String searchIaCode) {
		this.searchIaCode = searchIaCode;
	}

	public String getSearchNoticeId() {
		return searchNoticeId;
	}

	public void setSearchNoticeId(String searchNoticeId) {
		this.searchNoticeId = searchNoticeId;
	}

	public String getSearchKeywordType() {
		return searchKeywordType;
	}

	public void setSearchKeywordType(String searchKeywordType) {
		this.searchKeywordType = searchKeywordType;
	}

	public String getSearchKeywordTitle() {
		return searchKeywordTitle;
	}

	public void setSearchKeywordTitle(String searchKeywordTitle) {
		this.searchKeywordTitle = searchKeywordTitle;
	}

}
