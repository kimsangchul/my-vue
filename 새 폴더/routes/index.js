import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import DetailView from '../view/DetailView.vue';

export default new VueRouter({
	mode : 'history',
	routes : [
		{
			path : '/admin/item',
			component : DetailView,
		}
	],
});