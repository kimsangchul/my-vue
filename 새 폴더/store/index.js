import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import { selectListUrl, selectUserUrl, selectHackersNewsUrl, selectItemUrl, categoriesUrl, mergeUserUrl, mergeHackersNewsUrl, mergeItemUrl } from '../api/index.js';

export default new Vuex.Store({
	state : {
		list : [],
		hackersNews : {
			id : '',
			title : '',
			points : '',
			name : '',
			time : '',
			time_ago : '',
			comments_count : '',
			type : '',
			url : '',
			domain : '',
			category : '',
		},
		item : {
			content : '',
		},
		user : {
			about : '',
			created_time : '',
			created : '',
			id : '',
			karma : '',
		},
		categories : [],
		pageInfo : {
			category : '',
			filterOn : 'points',
			filter : '',
			perPage : 5,
			pageOptions : [5, 10, 15],
			currentPage : 1,
			totalRows : 0,
			sortBy : 'points',
			sortDesc : false,
			fields : [
				{
					key : 'category',
					label : '구분',
					sortable : true,
				},
				{
					key : 'points',
					label : '공헌도',
					sortable : true,
				},
				{
					key : 'title',
					label : '제목',
					sortable : true,
				},
				{
					key : 'name',
					label : '이름',
					sortable : true,
				},
				{
					key : 'time_ago',
					label : '시간',
					sortable : true,
				}
			],
		},
	},
	mutations : {
		SET_LIST(state, list) {
			state.list = list;
		},
		SET_USER(state, user) {
			state.user = user;
		},
		SET_HACKERSNEWS(state, hackersNews) {
			if(hackersNews.category === 'News') {
				hackersNews.category = '01';
			} else if(hackersNews.category === 'Ask') {
				hackersNews.category = '02';
			} else if(hackersNews.category === 'Jobs') {
				hackersNews.category = '03';
			}
			state.hackersNews = hackersNews;
		},
		SET_ITEM(state, item) {
			state.item = item;
		},
		SET_CATEGORIES(state, categories) {
			state.categories = categories;
		},
		SET_CATEGORY(state, category) {
			state.pageInfo.category = category;
		},
		SET_FILTERON(state, filterOn) {
			state.pageInfo.filterOn = filterOn;
		},
		SET_FILTER(state, filter) {
			state.pageInfo.filter = filter;
		},
		SET_PERPAGE(state, perPage) {
			state.pageInfo.perPage = perPage;
		},
		SET_CURRENTPAGE(state, currentPage) {
			state.pageInfo.currentPage = currentPage;
		},
		SET_TOTALROWS(state, totalRows) {
			state.pageInfo.totalRows = totalRows;
		},
	},
	actions : {
		async FETCH_LIST(context, params) {
			const response = await selectListUrl(params);
			
			const list = JSON.parse(response.data.d);
			context.commit('SET_LIST', list);
			
			return response;
		},
		async FETCH_USER(context, params) {
			const response = await selectUserUrl(params);
			
			const user = JSON.parse(response.data.d);
			context.commit('SET_USER', user);
			return response;
		},
		async FETCH_HACKERSNEWS(context, params) {
			const response = await selectHackersNewsUrl(params);
			
			const hackersNews = JSON.parse(response.data.d);
			context.commit('SET_HACKERSNEWS', hackersNews);
			return response;
		},
		async FETCH_ITEM(context, params) {
			const response = await selectItemUrl(params);
			
			const item = JSON.parse(response.data.d);
			context.commit('SET_ITEM', item);
			return response;
		},
		async FETCH_CATEGORIES(context) {
			const response = await categoriesUrl();
			const codeList = JSON.parse(response.data.d);
			let categories = [];
			codeList.forEach(v => {
				let map = new Map();
				map.set('text', v.NAME);
				map.set('value', v.CODE);
				const jsonObj = Object.fromEntries(map);
				categories.push(jsonObj);
			});
			context.commit('SET_CATEGORIES', categories);
			return response;
		},
		async MERGE_USER(context, params) {
			const response = await mergeUserUrl(params);
			return response;
		},
		async MERGE_HACKERSNEWS(context, params) {
			const response = await mergeHackersNewsUrl(params);
			return response.data.success;
		},
		async MERGE_ITEM(context, params) {
			const response = await mergeItemUrl(params);
			return response.data.success;
		},
	},
	getters : {
		getList(state) {
			return state.list;
		},
		getUser(state) {
			return state.user;
		},
		getHackersNews(state) {
			return state.hackersNews;
		},
		getItem(state) {
			return state.item;
		},
		getCategories(state) {
			return state.categories;
		},
		getPageInfo(state) {
			return state.pageInfo;
		},
	},
});